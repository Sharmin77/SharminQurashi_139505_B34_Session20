<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit140fc0109b238b3f64633fac0a2db0fc
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP139505',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit140fc0109b238b3f64633fac0a2db0fc::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit140fc0109b238b3f64633fac0a2db0fc::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
