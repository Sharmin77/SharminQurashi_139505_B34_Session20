<?php
require_once("../../vendor/autoload.php");
use App\Person;
use App\Student;


$objperson = new Person();
$objperson->showPersonInfo();

$objStudent = new Student();
$objStudent->showStudentInfo();
